from apiflask import Schema
from apiflask.fields import Integer, String, Boolean
from apiflask.validators import Length, Email, OneOf
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime, timedelta
import jwt

from app.extensions import db
from app.models.registration import Registration

####
## Schemas for OpenAPI and validation
####
class UserIn(Schema):
    username = String(required=True, validate=Length(0, 128))
    email = String(required=False, validate=[Length(0, 128), Email()])
    role = String(required=True, validate=OneOf(['user', 'admin']))
    password = String(required=True, validate=Length(8, 256))


class UserAuthIn(Schema):
    username = String(required=True, validate=Length(0, 128))
    password = String(required=True, validate=Length(8, 256))


class UserPassCheckIn(Schema):
    password = String(required=True, validate=Length(8, 256))


class UserOut(Schema):
    id = Integer()
    username = String()
    email = String()


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(128), unique=True, nullable=False)
    email = db.Column(db.String(128), unique=True, nullable=False)
    role = db.Column(db.String(15),nullable=False, default='user')
    password = db.Column(db.String(256), nullable=False)
    enabled = db.Column(db.Boolean, nullable=False, default=True)

    def set_password(self, password_clear):
        self.password = generate_password_hash(password_clear)

    def check_password(self, password_clear):
        return check_password_hash(self.password, password_clear)

    def generate_auth_token(self, expiration=600):
        token = jwt.encode({
            'username': self.username,
            'role': self.role,
            'iat': datetime.utcnow(),
            'exp': datetime.utcnow() + timedelta(minutes=30)
        }, "test", algorithm='HS256')
        return token

    def verify_auth_token(token):
        try:
            data = jwt.decode(token, 'test', algorithms=['HS256'])
            return User.query.filter_by(id=data['id']).first_or_404()
        except jwt.ExpiredSignatureError:
            return None
        except jwt.InvalidTokenError:
            return None
        except Exception as e:
            # Log or handle other exceptions
            return (f"An error occurred: {e}")
