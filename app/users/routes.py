from app.users import bp
from app.extensions import db
from app.models.user import User, UserIn, UserOut, UserPassCheckIn, UserAuthIn
from app.auth import token_auth


####
## view functions
####
@bp.get('/<int:user_id>')
@bp.auth_required(token_auth)
@bp.output(UserOut)
def get_user(user_id):
    return db.get_or_404(User, user_id)

@bp.get('/')
@bp.auth_required(token_auth)
@bp.output(UserOut(many=True))
def get_users():
    return User.query.all()

@bp.post('/')
@bp.auth_required(token_auth)
@bp.input(UserIn, location='json')
@bp.output(UserOut, status_code=201)
def create_user(json_data):
    user = User(**json_data)
    user.set_password(json_data['password'])
    db.session.add(user)
    db.session.commit()
    return user

@bp.patch('/<int:user_id>')
@bp.auth_required(token_auth)
@bp.input(UserIn(partial=True), location='json')
@bp.output(UserOut)
def update_user(user_id, json_data):
    user = db.get_or_404(User, user_id)
    for attr, value in json_data.items():
        if attr == 'password':
            user.set_password(json_data['password'])
        else:
            setattr(user, attr, value)
    db.session.commit()
    return user

@bp.delete('/<int:user_id>')
@bp.auth_required(token_auth)
@bp.output({}, status_code=204)
def delete_user(user_id):
    user = db.get_or_404(User, user_id)
    db.session.delete(user)
    db.session.commit()
    return ''

@bp.post('/<int:user_id>/check_password')
@bp.auth_required(token_auth)
@bp.input(UserPassCheckIn, location='json')
def post_check_password(user_id, json_data):
    user = db.get_or_404(User, user_id)
    password = json_data['password']
    if user.check_password(password):
        return {'message': 'Password is correct'}
    else:
        return {'message': 'Password is incorrect'}, 401


@bp.post('/login')
@bp.input(UserAuthIn, location='json')
def login_user(json_data):
    username = json_data['username']
    password = json_data['password']
    user = User.query.filter_by(username=username).first_or_404()
    if user.check_password(password) and user.enabled:
        token = user.generate_auth_token(600)
        return {'token': token}, 200
    else:
        return {'message': 'Incorrect username or password'}, 401


