from apiflask import HTTPTokenAuth
from app.models.user import User
from functools import wraps
from flask import abort

token_auth = HTTPTokenAuth()


@token_auth.verify_token
def token_required(token):
    user = User.verify_auth_token(token)
    if not user:
        return False
    return True
