from test import client
import pytest

def test_get_users(client):
    headers = { 'Authorization': 'Bearer {}'.format(auth_token) }
    response = client.get("/users/", headers=headers)
    assert response.status_code == 200
    assert response.json[2]['username'] == 'marcmuster'

def test_get_users(client, auth_token):
    headers = { 'Authorization': 'Bearer {}'.format(auth_token) }
    response = client.get("/users/2", headers=headers)
    assert response.status_code == 200
    assert response.json["username"] == "fredamueller"

def test_post_users(client, auth_token):
    headers = { 'Authorization': 'Bearer {}'.format(auth_token) }
    response = client.post("/users/", json={
            'username': 'johndoe', 'email': 'john.doe@testcorp.local', 'password': 'insecure', 'role': 'user'
        }, headers=headers)
    assert response.status_code == 201

def test_change_users(client, auth_token):
    headers = { 'Authorization': 'Bearer {}'.format(auth_token) }
    response = client.patch("/users/2", json={'password': 'insecure2'}, headers=headers)
    assert response.status_code == 200

